
#ifndef CLIENTTFTP_H
#define CLIENTTFTP_H

#include <QObject>
#include <QString>
#include <QUdpSocket>
#include <QHostAddress>
#include <QDataStream>
#include <QDebug>
#include <QFile>

typedef enum {
    TFTP_RRQ  =1, //!< Read request (RRQ)
    TFTP_WRQ  =2, //!< Write request (WRQ)
    TFTP_DATA =3, //!< Data (DATA)
    TFTP_ACK  =4, //!< Acknowledgment (ACK)
    TFTP_ERROR=5, //!< Error (ERROR)
    TFTP_OACK =6  //!< RFC 2347
} TFTP_OPCODE;


#define MAX_OPTIONS 16
#define MAX_TFTP_CLIENTS 100

class ClientTftp
{
public:
    ClientTftp();
    TFTP_OPCODE eOpcode;
    QHostAddress addr;
    QString pszFileName;
    QString pszMode;
    QFile* hFile;
    QByteArray pCache;
    bool fComplete=false;
    quint16 wBlockNumHigh=0;
    int nNextBlockNum=0;
    int nNumBlocks=0;
    int nBlockSize=512;
    int nTimeout=0;
    int nTSize=0;
    quint32 dwLastSignOfLife=0;
};

#endif // CLIENTTFTP_H
