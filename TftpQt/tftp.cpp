﻿#include "tftp.h"

tftp::tftp()
{

    socket = new QUdpSocket(this);
    connect(socket, &QUdpSocket::readyRead, this, &tftp::receivePacket);

    if (!socket->bind(QHostAddress::Any, portDefault)) {
        qDebug() << "Failed to bind to port" << portDefault;
    } else {
        qDebug() << "Listening on UDP port:" << socket->localPort();
    }
}

tftp::~tftp()
{
    if (socket) delete socket;
}

void tftp::receivePacket() {
    while (socket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        processPacket(datagram, sender, senderPort);
    }
}

void tftp::processPacket(QByteArray &datagram, const QHostAddress &sender, quint16 senderPort) {
    bool found = false;
    for (ClientTftp* clientInList : clientTftpList) {
        if (clientInList->addr == sender) {
            qDebug()<<"СТАРЫЙ КЛИЕНТА!!";
            client=clientInList;
            found = true;
            break; // Закончим поиск после первого совпадения, если необходимо
        }
    }
    if(!found){
        //Клиент не найден, создать нового
        qDebug()<<"НОВЫЙ КЛИЕНТА!!";
        client= new ClientTftp();
        //        client->eOpcode = ;
        client->addr = sender;
        client->pszFileName = "";
        client->pszMode = "octet";
        client->hFile = nullptr; // Инициализируйте его соответствующим образом
        client->pCache = nullptr; // Инициализируйте его соответствующим образом
        client->fComplete = false;
        client->wBlockNumHigh = 0;
        client->nNextBlockNum = 0;
        client->nNumBlocks = 0;
        client->nBlockSize = 512;
        client->nTimeout = 0;
        client->nTSize = 0;
        client->dwLastSignOfLife = 0;
        clientTftpList.append(client);
    }

    QDataStream stream(&datagram, QIODevice::ReadOnly);
    quint16 opCode;
    stream >> opCode;
    qDebug() << "datagram opCode-" << opCode;
    qDebug()<<QString("Received type %1 packet from %2:%3").arg(opCode).arg(sender.toString()).arg(senderPort);

    switch (opCode) {
    case 1: // RRQ - Read Request
        // Вызов функции, которая обрабатывает запрос на чтение файла
    case 2: // WRQ - Write Request
        // Вызов функции, которая обрабатывает запрос на запись файла
        StartTransfer(client,datagram,senderPort,sender,opCode);
        break;
    case 3: // DATA
        // Вызов функции, которая обрабатывает прием данных
        processTFTPData(client,datagram,senderPort);
        break;
    case 4: // ACK
        // Вызов функции, которая обрабатывает подтверждение приема данных
        processACKData(client,datagram, senderPort);
        break;
    case 5: // ERROR
        // Вызов функции, которая обрабатывает ошибку

        break;
    default:

        break;
    }
}

void tftp::processTFTPData(ClientTftp* clientIn,const QByteArray &datagram, quint16 senderPort) {
    if (datagram.size() < 4) {
        qDebug() << "Invalid TFTP packet size";
        return;
    }

    quint16 opcode;
    QDataStream in(datagram);
    in >> opcode;

    if (opcode == TFTP_DATA) {
        quint16 blockNum;
        in >> blockNum;
        qDebug()<<blockNum;
        if (clientIn->nNextBlockNum != ((clientIn->wBlockNumHigh << 16) | blockNum)) {
            qDebug()<<"returned "<<clientIn->nNextBlockNum<<" "<<clientIn->wBlockNumHigh<<" "<<blockNum;
            return;
        }
        clientIn->nNextBlockNum++;

        if (blockNum == 0xFFFF)
            clientIn->wBlockNumHigh++;

        QByteArray data = datagram.mid(4);
        //        qDebug() << "Data " << blockNum << " len" << data.size()<<"OP->"<<clientIn->eOpcode;
        //        printProgress(clientIn->nNumBlocks,blockNum);
        if (clientIn->eOpcode == TFTP_WRQ) {
            QFile file(clientIn->pszFileName);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Append)) {
                qDebug() << "Failed to open file for writing";
                return;
            }

            qint64 bytesWritten = file.write(data);
            if (bytesWritten == -1) {
                qDebug() << "Write failed";
                file.close();
                return;
            }

            qDebug() << "Bytes written:" << bytesWritten;

            if (bytesWritten < data.size()) {
                qDebug() << "Write failed: Disk full";
                file.close();
                return;
            }

            file.close();
            SendAck(socket, clientIn->addr,senderPort,blockNum);
            if (data.size() < clientIn->nBlockSize) {
                // Файл завершен
                qDebug() << "[complete]";
                releaseClient(clientIn);
                //                emit fileReceived();
            }
        } else {
            // Отправляем сообщение об ошибке: недопустимая операция
            qDebug() << "Illegal operation";
            releaseClient(clientIn);
        }
    }
}

bool tftp::createOrOverwriteFile(const QString &filename) {
    // Проверяем существует ли файл, и если да, то удаляем его
    if (QFile::exists(filename)) {
        QFile::remove(filename);
    }

    // Создаем новый файл
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate)) {
        // Файл успешно открыт и готов к использованию
        qDebug() << "File created or overwritten: " << filename;
        file.close();
    } else {
        // Ошибка при создании файла
        qDebug() << "Error creating or overwriting file: " << filename;
        return false;
    }
    return true;
}


void tftp::StartTransfer(ClientTftp* clientIn,QByteArray &datagram, quint16 senderPort, const QHostAddress &sender,quint16 eOpcode){
    // Извлекаем имя файла и режим из пакета
    QString filename;
    QString mode;

    int nFileNameLen = 0;
    int nModeLen = 0;
    int i = 2; // начинаем считывать с 3-го байта, так как первые два байта содержат опкод
    int nOptCount = 0;
    QList<QString> pszOutOptNames;
    QList<QString> pszOutOptValues;
    for (int i = 0; i < MAX_OPTIONS; ++i) {
        pszOutOptNames.append("");
    }
    for (int i = 0; i < MAX_OPTIONS; ++i) {
        pszOutOptValues.append("");
    }
    char pszTSize[20];
    int nOutOptCount = 0;

    // Считываем имя файла
    for (; i < datagram.size(); i++) {
        if (nFileNameLen < 256) // sizeof(pClient->pszFileName)
            filename.append(datagram.at(i));
        if (datagram.at(i) == 0)
            break;
    }
    filename.remove(QChar('\0'));
    i++;

    // Считываем режим
    for (; i < datagram.size(); i++) {
        if (nModeLen < 256) // sizeof(pClient->pszMode)
            mode.append(datagram.at(i));
        if (datagram.at(i) == 0)
            break;
    }

    mode.remove(QChar('\0'));

    clientIn->pszFileName=filename;
    clientIn->pszMode=mode;
    clientIn->eOpcode = TFTP_WRQ;
    clientIn->hFile = NULL;
    clientIn->nBlockSize = 512;
    clientIn->wBlockNumHigh = 0;
    clientIn->nNextBlockNum = 1;

    // Выводим информацию о файле и режиме
    qDebug() << "File: " << filename;
    qDebug() << "Mode: " << mode;
    if(!isValidFileName(filename)) {
        releaseClient(clientIn);
        return;
    }
    if(eOpcode == TFTP_WRQ)
    {
        if(!createOrOverwriteFile(filename)){
            releaseClient(clientIn);
        }

    } else {
        QFile file(filename);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << "Error opening file for reading: " << filename;
            releaseClient(clientIn);
            return;
        }

        clientIn->hFile=&file;
        // Получаем размер файла
        qint64 fileSize = clientIn->hFile->size();
        clientIn->nTSize=fileSize;
        qDebug()<<"fileSize->"<<fileSize;
        if (fileSize == -1 || fileSize == 0) {
            qDebug()<<"File size zero or too large (>2GB)";
            releaseClient(clientIn);
            return;
        }
        //Обновляем объем кэша, если файл до 32мб
        readFileIntoCache(clientIn);
    }
    // Объявляем переменные для хранения опций и их значений
    QList<QString> pszOptName;
    QList<QString> pszOptVal;
    for (int i = 0; i < MAX_OPTIONS; ++i) {
        pszOptName.append("");
    }
    for (int i = 0; i < MAX_OPTIONS; ++i) {
        pszOptVal.append("");
    }

    while (i < datagram.size()) {
        while (i < datagram.size() && datagram.at(i) != '\0') {
            i++;
        }
        i++; // Пропускаем нулевой байт

        // Считываем значение опции
        while (i < datagram.size() && datagram.at(i) != '\0') {
            pszOptName.append(QString(datagram.at(i)));
            i++;
        }
        i++; // Пропускаем нулевой байт
        while (i < datagram.size() && datagram.at(i) != '\0') {
            pszOptVal.append(QString(datagram.at(i)));

            i++;
        }
        i++; // Пропускаем нулевой байт
        qDebug()<<"pszOptName->"<<pszOptName<<" pszOptVal->"<<pszOptVal;
        // Обработка опций в зависимости от их значения
        QString res=pszOptName.join("");
        if (res.compare("blksize", Qt::CaseInsensitive) == 0) {
            qDebug()<<"Опция blksize";
            // Обработка опции blksize
            pszOutOptNames[nOutOptCount] = pszOptName.join("");
            pszOutOptValues[nOutOptCount] = pszOptVal.join("");
            nOutOptCount++;
            qDebug()<<"QString(pszOptVal[nOptCount]).toInt()->"<<QString(pszOptVal[nOptCount]).toInt();
            clientIn->nBlockSize = QString(pszOptVal.join("")).toInt();
            if((clientIn->nBlockSize<8) || (clientIn->nBlockSize>65464)) {
                releaseClient(clientIn);
                return;
            }
        }
        else if (res.compare("timeout", Qt::CaseInsensitive) == 0) {
            qDebug()<<"Опция timeout";
            // Обработка опции timeout
            pszOutOptNames[nOutOptCount] = pszOptName.join("");
            pszOutOptValues[nOutOptCount] = pszOptVal.join("");
            nOutOptCount++;
            clientIn->nTimeout = QString(pszOptVal.join("")).toInt();
            if((clientIn->nTimeout<1) || (clientIn->nTimeout>255)) {
                releaseClient(clientIn);
                return;
            }

        }
        else if (res.compare("tsize", Qt::CaseInsensitive) == 0) {
            qDebug()<<"Опция tsize";
            // Обработка опции tsize
            pszOutOptNames[nOutOptCount] = pszOptName.join("");
            pszOutOptValues[nOutOptCount] = pszOptVal.join("");
            if(eOpcode == TFTP_RRQ)
            {
                if(QString(pszOptVal.join("")).toInt()!=0) {
                    releaseClient(clientIn);
                    return;
                } else {
                    pszOutOptValues[nOutOptCount] = QString(pszTSize)[0];
                }
            } else {
                clientIn->nTSize = QString(pszOptVal.join("")).toInt();
                if(clientIn->nTSize<=0) {
                    releaseClient(clientIn);
                    return;
                }
            }
            nOutOptCount++;
        }
        else {
            // Опция не распознана
            qDebug() << "Option" << pszOptName << "=" << pszOptVal << "not recognized";
        }

        // Вывод опций в консоль (для отладки)
        qDebug() << pszOptName << "=" << pszOptVal;
        nOutOptCount++;
    }





    clientIn->nNumBlocks = clientIn->nTSize/clientIn->nBlockSize;
    if(clientIn->nTSize % clientIn->nBlockSize){
        clientIn->nNumBlocks++;
    }
    qDebug()<<"clientIn->nNumBlocks ->"<<clientIn->nNumBlocks ;
    if(nOutOptCount){
        qDebug() << pszOutOptNames << "=" << pszOutOptValues;
        SendOACK(clientIn,senderPort, pszOutOptNames, pszOutOptValues, nOutOptCount);
    }
    if(eOpcode == TFTP_WRQ) {
        SendAck(socket, sender,senderPort,0);
    }else{
        SendNextDataBlock(1,clientIn,senderPort);
    }
}

void tftp::SendOACK(ClientTftp* clientIn, quint16 senderPort, QList<QString> &optNames, QList<QString> &optValues, int nOptions) {
    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::WriteOnly);

    // Записываем код операции TFTP (OACK) в datagram
    out << quint16(TFTP_OACK);
    qDebug()<<"optNames->"<<optNames<<" optValues->"<<optValues;
    // Добавляем опции
    for (int i = 0; i < nOptions; ++i) {
        QByteArray name = optNames[i].toUtf8();
        QByteArray value =  optValues[i].toUtf8();
        // Записываем имя опции
        datagram.append(name);
        datagram.append('\0');
        // Записываем значение опции
        datagram.append(value);
        datagram.append('\0');
    }

    // Отправляем datagram обратно клиенту по указанному адресу и порту
    socket->writeDatagram(datagram, clientIn->addr, senderPort);
}

void tftp::printProgress(int allBlocks, int blocksSended)
{
    // Вычисляем процент выполнения
    int progressPercentage = (static_cast<double>(blocksSended) / allBlocks) * 100;

    // Выводим в консоль процент выполнения
    qDebug() << "Progress:" << progressPercentage << "%";

    // Вычисляем количество символов, которые должны быть отображены в прогресс-баре
    int numSymbols = 20; // Общее количество символов в прогресс-баре
    int completedSymbols = static_cast<double>(blocksSended) / allBlocks * numSymbols;
    int remainingSymbols = numSymbols - completedSymbols;

    // Выводим в консоль прогресс-бар
    QString progressBar = QString("[");
    progressBar.append(QString(completedSymbols, '='));
    progressBar.append(QString(remainingSymbols, ' '));
    progressBar.append("]");
    qDebug() << "Progress:" << progressBar;
}

void tftp::SendNextDataBlock(int nBlockNum, ClientTftp* clientIn, quint16 senderPort) 
{ 
    quint32 dwRead = 0; 
    QByteArray bData; 
 
    if (nBlockNum < 1) 
        return; 
 
    QFile file(clientIn->pszFileName); 
    if (!file.open(QIODevice::ReadOnly)) { 
        qDebug() << "Failed to open file"; 
        releaseClient(clientIn); 
        emit Error(); 
        return; 
    } 
 
    // Вычисляем количество байтов для чтения 
    dwRead = qMin(static_cast<quint32>(clientIn->nBlockSize), static_cast<quint32>(clientIn->nTSize - (nBlockNum - 1) * clientIn->nBlockSize)); 
    if (dwRead > 0) { 
        // Перемещаем указатель файла на нужное место 
        if (!file.seek((nBlockNum - 1) * clientIn->nBlockSize)) { 
            qDebug() << "Failed to seek file"; 
            releaseClient(clientIn); 
            emit Error(); 
            return; 
        } 
 
        // Читаем данные из файла 
        bData = file.read(dwRead); 
    } 
 
    qDebug()<<"bdata->"<<bData; 
    SendData(socket, clientIn, nBlockNum, bData,senderPort); 
}

void tftp::processACKData(ClientTftp *clientIn, QByteArray &datagram, quint16 senderPort)
{
    if (datagram.size() < 4) {
        qDebug() << "Invalid TFTP packet size";
        releaseClient(clientIn);
        return;
    }
    QDataStream stream(&datagram, QIODevice::ReadOnly);
    quint16 opCode;
    quint16 wBlockNum;
    stream >>opCode>>wBlockNum;
    qDebug() << "Ack data:"<< wBlockNum;
    clientIn->nNextBlockNum = ((clientIn->wBlockNumHigh<<16)|wBlockNum);
    qDebug() << "nNextBlockNum:"<< clientIn->nNextBlockNum<<"  clientIn->nNumBlocks "<<clientIn->nNumBlocks;
    printProgress(clientIn->nNumBlocks,clientIn->nNextBlockNum);
    if((clientIn->nNextBlockNum<clientIn->nNumBlocks) && (clientIn->nNumBlocks!=1)) {
        if(wBlockNum == 0xFFFF){
            SendNextDataBlock(((clientIn->wBlockNumHigh+1)<<16)|(0),clientIn,senderPort);
        }
        else
        {
            SendNextDataBlock(((clientIn->wBlockNumHigh)<<16)|(wBlockNum+1),clientIn,senderPort);
        }
    }
    else {
        if(wBlockNum != 0) {
            clientIn->fComplete = true;
            qDebug()<<"[complete]\r\n";
            releaseClient(clientIn);
        }
    }
    if(wBlockNum == 0xFFFF){
        clientIn->wBlockNumHigh++;
    }
}

void tftp::releaseClient(ClientTftp *clientIn)
{
    qDebug()<<"Удаляю клиента->"<<clientIn->addr;
        // Находим индекс элемента в списке
        int index = clientTftpList.indexOf(clientIn);
    if (index != -1) {
        // Удаляем элемент из списка
        clientTftpList.removeOne(clientIn);
        // Освобождаем память, выделенную под объект
        delete clientIn;
    }
}

void tftp::SendData(QUdpSocket *socket, ClientTftp* clientIn, int nBlockNum, const QByteArray pData, quint16 senderPort)
{
    QByteArray bData;
    QDataStream out(&bData, QIODevice::WriteOnly);

    // Записываем заголовок TFTP DATA пакета
    out << quint16(TFTP_DATA);
    out << quint16(nBlockNum);

    // Добавляем данные
    bData.append(pData);

    // Отправляем пакет
    socket->writeDatagram(bData, clientIn->addr, senderPort);
}

void tftp::SendAck(QUdpSocket *socket, const QHostAddress &sender, quint16 senderPort, int nBlockNum) {
    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::WriteOnly);

    // Записываем код операции TFTP (ACK) и номер блока в datagram
    out << (quint16)0x04 << (quint16)nBlockNum;

    // Отправляем datagram обратно клиенту по указанному адресу и порту
    socket->writeDatagram(datagram, sender, senderPort);
}

int tftp::calculateNumBlocks(int fileSize, int blockSize) {
    // Вычисляем количество блоков
    int numBlocks = fileSize / blockSize;
    if (fileSize % blockSize != 0)
        numBlocks++;

    // Выводим результат
    qDebug() << "Number of blocks: " << numBlocks;
    return numBlocks;
}

bool tftp::readFileIntoCache(ClientTftp* clientIn) {
    // Открываем файл для чтения
    QFile file(clientIn->pszFileName);
    if (!file.open(QIODevice::ReadOnly)) {
        // Если файл не удалось открыть, выводим сообщение об ошибке и выходим из функции
        qDebug() << "Error opening file for reading: " << clientIn->pszFileName;
        return false;
    }

    // Выделяем память под буфер для кэширования данных
    qDebug()<<"clientIn->nTSize ->"<<clientIn->nTSize ;
    if (clientIn->nTSize < FILE_CACHE_LIMIT) {
        QByteArray cache;
        cache.resize(clientIn->nTSize);
        clientIn->pCache=cache;
        // Читаем данные из файла в кэш
        if (file.read(cache.data(), clientIn->nTSize) != clientIn->nTSize) {
            // Если произошла ошибка чтения файла, выводим сообщение об ошибке и выходим из функции
            qDebug() << "File read error";
            return false;
        }

        // Файл прочитан успешно, можно продолжить обработку данных из кэша

        // Закрываем файл
        file.close();
    } else {
        // Если размер файла превышает предельный размер кэша, выходим с ошибкой
        qDebug() << "File size exceeds cache limit";
        //        releaseClient(clientIn);
        return false;
    }
    return true;
}

bool tftp::isValidFileName(const QString &fileName) {
    if (fileName.startsWith("./"))
        return true;

    if (fileName.contains(".."))
        return false;

    if (fileName.startsWith("/") || fileName.startsWith("\\"))
        return false;

    if (fileName.contains(":"))
        return false;

    return true;
}
