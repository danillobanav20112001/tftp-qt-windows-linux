
#ifndef TFTP_H
#define TFTP_H

#include <QCoreApplication>

#define MAXPACKETSIZE 512
#include <QObject>
#include <QString>
#include <QUdpSocket>
#include <QHostAddress>
#include <QDataStream>
#include <QDebug>
#include <QFile>
//#include "tftpserver.h"
#include "clienttftp.h"
#define portDefault 69
#define FILE_CACHE_LIMIT 32*1024*1024
#define MAX_UDP_PACKETSIZE 65535
class tftp : public QObject
{
    Q_OBJECT

public:
    tftp();
    ~tftp();
    QList<ClientTftp*> clientTftpList;
    ClientTftp* client;
    QUdpSocket *socket;
    bool StopServerTftp=false;
    bool isValidFileName(const QString &fileName);
    bool createOrOverwriteFile(const QString &filename);
    bool readFileIntoCache(ClientTftp* clientIn);
    int calculateNumBlocks(int fileSize, int blockSize);
    void SendAck(QUdpSocket *socket, const QHostAddress &sender, quint16 senderPort, int nBlockNum);
    void processTFTPData(ClientTftp* clientIn,const QByteArray &datagram, quint16 senderPort);
    void SendData(QUdpSocket *socket, ClientTftp* clientIn, int nBlockNum, const QByteArray pData,quint16 senderPort);
    void SendNextDataBlock(int nBlockNum, ClientTftp* clientIn, quint16 senderPort);
    void processACKData(ClientTftp* clientIn,QByteArray &datagram, quint16 senderPort);
    void releaseClient(ClientTftp* clientIn);
    void SendOACK(ClientTftp* clientIn, quint16 senderPort, QList<QString> &optNames, QList<QString> &optValues, int nOptions);
public slots:
    void receivePacket();
   void processPacket(QByteArray &datagram, const QHostAddress &sender, quint16 senderPort);
    void StartTransfer(ClientTftp* clientIn,QByteArray &datagram, quint16 senderPort, const QHostAddress &sender,quint16 eOpcode);
//    void startServer();
//    void StopServer();
signals:
    void serverStarted();
    void fileSent();
    void fileReceived();
    void Error();
    void finished();
    void returnProgress(float data,QString dataStr,bool modbusFalse);
};

#endif // TFTP_H
