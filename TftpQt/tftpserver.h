
#ifndef TFTPSERVER_H
#define TFTPSERVER_H
#include <QUdpSocket>
#include <QHostAddress>
#include <QByteArray>
#include <QString>
#include <QFile>
#include <QIODevice>
#include <QDataStream>
#include <QDebug>


#include "clienttftp.h"

class TftpServer : public QObject
{
    Q_OBJECT
public:
    explicit TftpServer(QObject *parent = nullptr) : QObject(parent) {}

    void sendAck(const QHostAddress &address, quint16 port, quint16 blockNum) {
        QByteArray data;
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream.setVersion(QDataStream::Qt_5_15);
        stream << quint16(TFTP_ACK) << blockNum;

        m_socket.writeDatagram(data, address, port);
    }

    void sendErr(const QHostAddress &address, quint16 port, quint16 errorCode, const QString &errorMsg) {
        QByteArray data;
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream.setVersion(QDataStream::Qt_5_15);
        stream << quint16(TFTP_ERROR) << errorCode << errorMsg.toUtf8();

        m_socket.writeDatagram(data, address, port);
    }

    void sendData(const QHostAddress &address, quint16 port, quint16 blockNum, const QByteArray &fileData) {
        QByteArray data;
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream.setVersion(QDataStream::Qt_5_15);
        stream << quint16(TFTP_DATA) << blockNum << fileData;

        m_socket.writeDatagram(data, address, port);
    }

    void sendOack(const QHostAddress &address, quint16 port, const QStringList &optNames, const QStringList &optValues) {
        QByteArray data;
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream.setVersion(QDataStream::Qt_5_15);
        stream << quint16(TFTP_OACK);

        for (int i = 0; i < optNames.size(); ++i) {
            stream << optNames[i].toUtf8() << optValues[i].toUtf8();
        }

        m_socket.writeDatagram(data, address, port);
    }



    void sendNextDataBlock(int blockNum, const QByteArray &fileData, const QHostAddress &address, quint16 port) {
        qint64 blockSize = fileData.size();
        qint64 offset = (blockNum - 1) * blockSize;
        QByteArray blockData = fileData.mid(offset, blockSize);

        sendData(address, port, blockNum, blockData);
    }

private:
    QUdpSocket m_socket;
};

#endif // TFTPSERVER_H
